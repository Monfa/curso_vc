#importing libraries
import numpy as np
import cv2 as cv

#loading an image as it is stored in the disk.
f = cv.imread('images/gradient.tif', cv.IMREAD_UNCHANGED)

threshold = 150

# 1) thresholding an image using for loop
shape_f = np.shape(f)
g = np.zeros(shape_f)
for j in range(0,shape_f[1]):
    for i in range(0,shape_f[0]):
        if(f[i,j]<threshold):
            g[i,j] = 0
        else:
            g[i,j] = 255

cv.imwrite('output/binary_transf_1.png', g)

# 2) thresholding an image using conditional element-wise operation
g = (f >= threshold)*255

#saving images using OpenCV
cv.imwrite('output/binary_transf_2.png', g)