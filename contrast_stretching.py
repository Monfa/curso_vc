#importing libraries
import numpy as np
import cv2 as cv

#loading an image as it is stored in the disk.
f = cv.imread('images/empire_br_low_contrast.tif', cv.IMREAD_UNCHANGED)

f1 = 100
g1 = 50
f2 = 150
g2 = 200
L = 256

#contrast stretching operation through image
shape_f = np.shape(f)
g = np.zeros(shape_f)
for j in range(0,shape_f[1]):
    for i in range(0,shape_f[0]):
        if(f[i,j]<=f1):
            g[i,j]=f[i,j]*g1/f1
        elif(f[i,j]>f1 and f[i,j]<=f2):
            g[i,j]=(f[i,j]-f1)*(g2-g1)/(f2-f1)+g1
        elif(f[i,j]>f2):
            g[i,j]=(f[i,j]-f2)*((L-1)-g2)/((L-1)-f2)+g2

#saving images using OpenCV
cv.imwrite('output/contrast_stretching.png', g)