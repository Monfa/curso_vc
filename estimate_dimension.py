#importing libraries
import pickle
import matplotlib.pyplot as plt 
import cv2 as cv
import numpy as np

#loading camera parameters
with open('calibration/camera_params.pkl', 'rb') as file_in:
    M, Ks = pickle.load(file_in)

#loading object to be measured
img_dims = cv.imread('calibration/object_dims.jpg', cv.IMREAD_UNCHANGED)  
img_dims = cv.cvtColor(img_dims, cv.COLOR_BGR2RGB)
plt.imshow(img_dims)

#capturing four points ON THE IMAGE (in pixels) using ginput
#for the circular pattern used this points (x,y) are
#xc1 (left) and xc2 (right) forms the horizonta diameter
pts = plt.ginput(2) 
xc1 = np.array([[pts[0][0]], [pts[0][1]], [1]])
xc2 = np.array([[pts[1][0]], [pts[1][1]], [1]])

#distance from camera to object (in meters)
#this is the information known from the
#environment in the camera's coordinate system
Zc = 0.242

#we have to calculate the inverse because we
#now want to make the inverse path from a value
#in the image to a value in the real world
M_inv = np.linalg.inv((1/Zc)*M)

#obtaining the two image points in the CAMERA'S 
#coordinate system, thus in meters
XC1 = np.matmul(M_inv,xc1)
XC2 = np.matmul(M_inv,xc2)

#calculating diameters (in meters)
diameter = np.sqrt(pow(XC1[0]-XC2[0],2)+pow(XC1[1]-XC2[1],2))

#printing
print('Diameter', diameter,' meters')
