#importing libraries
import numpy as np
import cv2 as cv

#loading an image as it is stored in the disk.
img = cv.imread('images/gradient.tif', cv.IMREAD_UNCHANGED)

output_size = img.shape

#resizing image
img_rsz_x0_1 = cv.resize(img, dsize=None, fx=0.1, fy=0.1, interpolation = cv.INTER_NEAREST)
img_rsz_x10 = cv.resize(img, dsize=None, fx=10, fy=10, interpolation = cv.INTER_NEAREST)

#saving images
cv.imwrite('output/resizedx_0_1.png', img_rsz_x0_1)
cv.imwrite('output/resizedx_1.png', img)
cv.imwrite('output/resizedx_10.png', img_rsz_x10)

# from matplotlib import pyplot as plt
# import utils as ut
# region = [115,160,130,175]
# zoom = 2
# fig1 = ut.zoom_region_image(img, region, zoom, cmap=plt.gray())
# fig2 = ut.zoom_region_image(img_rsz_x10, np.multiply(region, 10), zoom, cmap=plt.gray())
# fig1.savefig('output/resizedx_1_zoomed.png', dpi=300, bbox_inches='tight')
# fig2.savefig('output/resizedx_10_zoomed.png', dpi=300, bbox_inches='tight')
