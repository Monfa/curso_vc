# -*- coding: iso-8859-15 -*-
#to accept accentuation
#importing libraries
import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

#loading an image as it is stored in the disk.
img = cv.imread('images/pepitas.png', cv.IMREAD_UNCHANGED)

#converting RGB image to grayscale
img_gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

#thresholding
img_bin = img_gray < 100

fig = plt.figure()
plt.imshow(img, cmap='gray', vmin=0)
ax = plt.gca()
ax.axes.xaxis.set_visible(False)
ax.axes.yaxis.set_visible(False)
plt.show(block=False)

fig_gray = plt.figure()
plt.imshow(img_gray, cmap='gray', vmin=0)
ax = plt.gca()
ax.axes.xaxis.set_visible(False)
ax.axes.yaxis.set_visible(False)
plt.show(block=False)

fig_bin = plt.figure()
plt.imshow(img_bin, cmap='gray', vmin=0)
ax = plt.gca()
ax.axes.xaxis.set_visible(False)
ax.axes.yaxis.set_visible(False)
plt.show()