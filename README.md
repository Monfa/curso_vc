Códigos fonte da disciplina de Programação Aplicada à Visão Computacional do curso de Oficinas 4.0.

Proibida a distribuição ou uso comercial das imagens desse repositório. Algumas imagens possuem licenças próprias, sendo o devido crédito realizado na apostila do curso.
