#importing libraries
import cv2 as cv

#loading an image as it is stored in the disk.
img = cv.imread('images/pompeii.tif', cv.IMREAD_UNCHANGED)  
cv.imshow('Pompeii City',img)
#avoiding closing image after being shown.
#please, select the image and press any keyboard key to close.
cv.waitKey(0)

#closing all opened windows.
cv.destroyAllWindows() 