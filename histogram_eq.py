# -*- coding: iso-8859-15 -*-
#to accept accentuation
#importing libraries
import cv2 as cv
from matplotlib import pyplot as plt
import numpy as np

#function to compute the histogram
def da_hist(img, L = 256):
    #L represents the number of bins
    
    #getting image shape and initializing hist
    shape_img = np.shape(img)
    h = np.zeros(L)
    #counting the number of intensities levels
    for j in range(0,shape_img[1]):
        for i in range(0,shape_img[0]):
            h[img[i,j]] =  h[img[i,j]] + 1
    #normalizing histogram
    p = np.divide(h,shape_img[0]*shape_img[1])

    return p

#number of intensities levels for 'uint8' data type
L = 256
fk = range(0, L)

#loading image, equalizing and computing histogram
f = cv.imread('images/pompeii.png', cv.IMREAD_UNCHANGED)
f_eq = cv.equalizeHist(f)
p_eq = da_hist(f_eq, L)

#generating histogram plot
fig = plt.figure()
plt.bar(fk, p_eq)
plt.ylabel("Ocorrências", fontsize=14)
plt.xlabel("Níveis de intensidade", fontsize=14)

#saving images to a directory
cv.imwrite('output/pompeii_eq.png',f_eq)
fig.savefig('output/pompeii_hist_eq.pdf', dpi=300, bbox_inches='tight')