#importing libraries
import cv2 as cv
import sys

#defining tracker object
tracker = cv.TrackerKCF_create()

#reading video
cap = cv.VideoCapture('videos/robot_tracking.mp4')

#reading first frame.
__, frame = cap.read()

#selecting an initial bounding box
bbox = cv.selectROI(frame, False)

#initializing tracker with first frame and bounding box
tracker.init(frame, bbox)

#defining the codec and creating VideoWriter object
#to register tracking output in a video
fourcc = cv.VideoWriter_fourcc(*'XVID')
vid_shape = (frame.shape[1],frame.shape[0])
video_out = cv.VideoWriter('output/robot_kcf_tracking_out.avi',fourcc, 30.0, vid_shape)

while True:
    
    #reading a new frame
    ret_cam, frame = cap.read()
    
    #exiting loop if the video ends
    if not ret_cam:
        break

    #updating tracker
    ret_tckr, bbox = tracker.update(frame)

    p1 = (int(bbox[0]), int(bbox[1]))
    p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
    cv.rectangle(frame, p1, p2, (255,0,0), 2, 1)

    #displaying result
    cv.imshow("Tracking", frame)

    #writing frame to the recorded video
    video_out.write(frame)

    #exiting if ESC pressed
    k = cv.waitKey(1) & 0xff
    if k == 27 : break

#releasing everything if job is finished
cap.release()
video_out.release()
cv.destroyAllWindows()
