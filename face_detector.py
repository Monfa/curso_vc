#importing libraries
import cv2 as cv

#loading trained classifier.
face_cascade = cv.CascadeClassifier('models/haarcascade_frontalface_default.xml')

#loading an image as it is stored in the disk.
img = cv.imread('images/chaves.jpg')

#converting RGB image to grayscale
img_gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

#detecting faces in pyramid of images
faces = face_cascade.detectMultiScale(img_gray, 1.1, 5)

#draw rectangle around the faces
for (x, y, w, h) in faces:
    cv.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 2)

#saving images using OpenCV
cv.imwrite('output/img_faces.png', img)