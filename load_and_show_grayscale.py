#importing libraries
import cv2 as cv
from matplotlib import pyplot as plt

#loading an image as it is stored in the disk.
img = cv.imread('images/pompeii.tif', cv.IMREAD_UNCHANGED)
#Now, using matplotlib to show the image
plt.imshow(img, cmap = 'gray')
plt.colorbar(cmap = 'gray',fraction=0.03, pad=0.04)
plt.show()
