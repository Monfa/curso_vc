#importing libraries
import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

#loading an image as it is stored in the disk.
img = cv.imread('images/vitral.png', cv.IMREAD_UNCHANGED)
img_hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)

#inspecting image using a for loop
shape_f = np.shape(img_hsv)
img_bin = np.zeros(shape_f[0:2])
for j in range(0,shape_f[1]):
    for i in range(0,shape_f[0]):
        #selecting just pixels related to blue
        # with saturation bigger than 50 (avoiding white)
        # with value bigger than 50 (not dark)
        if(img_hsv[i,j,0]>102
        and img_hsv[i,j,0]<138
        and img_hsv[i,j,1]>25
        and img_hsv[i,j,2]>25):
            img_bin[i,j] = 1
        else:
            img_bin[i,j] = 0

fig = plt.figure()
#for exibition since opencv is BGB based
#and Matplotlib is RGB based
img_rgb = cv.cvtColor(img, cv.COLOR_BGR2RGB)
plt.imshow(img_rgb, cmap='gray', vmin=0)
ax = plt.gca()
ax.axes.xaxis.set_visible(False)
ax.axes.yaxis.set_visible(False)
plt.tight_layout()
plt.show(block=False)

fig_bin = plt.figure()
plt.imshow(img_bin, cmap='gray', vmin=0)
ax = plt.gca()
ax.axes.xaxis.set_visible(False)
ax.axes.yaxis.set_visible(False)
plt.tight_layout()
plt.show()