#importing libraries
from PIL import Image

#loading an image using Pillow Image Class
img = Image.open('images/street_market_italy.jpg')

#cropping region of the image using Pillow Image Class
region = (20, 20, 100, 100)
img_crop = img.crop(region)

#rotating image using Pillow Image Class
img_rot = img.rotate(45)

#showing images using Pillow Image Class
img_crop.show(title = 'Street Market Italy Cropped')
img_rot.show(title = 'Street Market Italy Rotated')

#saving images using Pillow Image Class
img_crop.save('output/street_market_italy_crop.png')
img_rot.save('output/street_market_italy_rot.png')