#importing libraries
import cv2 as cv
import numpy as np
import utils as ut

#loading a binary image a20s it is stored in the disk.
img = cv.imread('images/OpenCV_Chessboard.png', cv.IMREAD_UNCHANGED)
img_gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)

#using the Harris corner detector
img_gray = np.float32(img_gray)
img_corners = cv.cornerHarris(img_gray,2,3,0.04)

#result is dilated for better visualization
img_corners = cv.dilate(img_corners, None, iterations=10)

#threshold for an optimal value
#just corners higher than threshold are kept
#making corners look red
img[img_corners>0.01*img_corners.max()]=[0,0,255]

#saving image
cv.imwrite('output/OpenCV_Chessboard_corners.png', img)
