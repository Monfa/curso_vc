#importing libraries
import cv2 as cv
import math

#loading images as they are stored in the disk
img = cv.imread('images/pompeii.tif', cv.IMREAD_UNCHANGED) 
img_rgb = cv.imread('images/napoli_mall.jpg', cv.IMREAD_UNCHANGED)  

#getting coordinates of the center for the grayscale image
yc = math.floor(img.shape[0]/2)
xc = math.floor(img.shape[1]/2)

#selecting an interval of the image grayscale around center
interval = 350
img_sel = img[int(yc-interval/2):int(yc+interval/2), int(xc-interval/2):int(xc+interval/2)]

#showing images
cv.imshow('Pompeii City',img)
cv.imshow('Pompeii City Selected',img_sel)

#saving images using OpenCV
cv.imwrite('output/pompeii.png', img)
cv.imwrite('output/pompeii_selected.png', img_sel)

#selecting part of the rgb image and specific channels
img_sel_rgb = img_rgb[0:10,0:10,[0,2]]
print(img_sel_rgb.shape)

#avoiding closing image after being shown.
#please, select the image and press any keyboard key to close.
cv.waitKey(0)
#closing all opened windows.
cv.destroyAllWindows()