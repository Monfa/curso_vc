#importing libraries
import numpy as np
import cv2 as cv

#reading video
cap = cv.VideoCapture('videos/tracking_colors.mp4')

#taking first frame of the video
__, frame = cap.read()

#selecting an initial bounding box
tck_win = cv.selectROI(frame, False)

#set up ROI for tracking
p1 = (int(tck_win[0]), int(tck_win[1]))
p2 = (int(tck_win[0] + tck_win[2]), int(tck_win[1] + tck_win[3]))
roi = frame[p1[1]:p2[1], p1[0]:p2[0],:]

#it is a color-base tracking - HSV color space is used
#mask helps tracking to focus in the desired color
hsv_roi =  cv.cvtColor(roi, cv.COLOR_BGR2HSV)

#mask to filter image in the desired range of colors
h_min = 0.
h_max = 25.
s_min = 80.
v_min = 80.
mask = cv.inRange(hsv_roi, np.array((h_min, s_min,v_min)), np.array((h_max,255.,255.)))

#histogram is used to generate the distribution
#of the desired color
roi_hist = cv.calcHist([hsv_roi],[0],mask,[180],[0,180])
cv.normalize(roi_hist,roi_hist,0,255,cv.NORM_MINMAX)

#Setup the termination criteria, either 10 iteration
#or move by at least 1 pt
term_crit = ( cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 1 )

#defining the codec and creating VideoWriter object
#to register tracking output in a video
fourcc = cv.VideoWriter_fourcc(*'XVID')
vid_shape = (frame.shape[1],frame.shape[0])
video_out = cv.VideoWriter('output/robot_slave_camshift_tracking_out.avi',fourcc, 30.0, vid_shape)

while(True):

    #reading a new frame
    ret_cam, frame = cap.read()
 
    #exiting loop if the video ends
    if not ret_cam:
        break

    #computing distribution tracked by camshift
    #camshift looks for the center of mass of 
    #this distribution
    hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
    dst = cv.calcBackProject([hsv],[0],roi_hist,[0,180],1)

    #applying camshift to get the new location
    ret, tck_win = cv.CamShift(dst, tck_win, term_crit)

    #drawing it on image
    pts = cv.boxPoints(ret)
    pts = np.int0(pts)
    frame = cv.polylines(frame,[pts],True, 255,2)
    cv.imshow('Tracking',frame)

    #writing frame to the recorded video
    video_out.write(frame)

    #exiting if ESC pressed
    k = cv.waitKey(60) & 0xff
    if k == 27:
        break

#releasing everything if job is finished
cap.release()
video_out.release()
cv.destroyAllWindows()