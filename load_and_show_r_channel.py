#importing libraries
import cv2 as cv

#loading an image as it is stored in the disk.
img = cv.imread('images/street_market_italy.jpg', cv.IMREAD_UNCHANGED)  

#Observe that OpenCV deals with the RGB in the BGR format.
#Channel 1: Blue, Channel 2: Green and Channel 3: Red.
#It is just a matter of order.
cv.imshow('Street Market Italy (Channel R)',img[:,:,2])

#avoiding closing image after being shown.
#please, select the image and press any keyboard key to close.
cv.waitKey(0)
#closing all opened windows.
cv.destroyAllWindows()