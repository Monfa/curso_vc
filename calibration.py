#importing libraries
import numpy as np
import cv2 as cv
import glob
import pickle

#chessboard number of internal corners in the vertical
#and horizontal directions
chess_shape = (8,6)

#preparing object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
square_size=0.030 #meters
objp = square_size*np.zeros((chess_shape[0]*chess_shape[1],3), np.float32)
objp[:,:2] = np.mgrid[0:chess_shape[0],0:chess_shape[1]].T.reshape(-1,2)

#arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

#calibration imagens
images = glob.glob('calibration/calib_images/*.jpg')

#termination criteria for the subpixel estimation
#sub-pixel provides robustenss to the calibration
#procedure
criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)

for fname in images:
    img = cv.imread(fname)
    gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)

    # Find the chess board corners
    ret, corners = cv.findChessboardCorners(gray, (8,6), True)

    # If found, add object points, image points (after refining them)
    if ret == True:
        objpoints.append(objp)

        corners2 = cv.cornerSubPix(gray,corners,(5,5),(-1,-1),criteria)
        imgpoints.append(corners2)

        # Draw and display the corners
        img = cv.drawChessboardCorners(img, (8,6), corners2,ret)
        cv.imshow('img',img)
        cv.waitKey(500)

cv.destroyAllWindows()

#compute camera parameters
#M - intrinsic parameters matrix
#Ks - distortion coefficients 
_, M, Ks, _, _ = cv.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)

#file to save parameters
with open('calibration/camera_params.pkl', 'wb') as file_out:
    pickle.dump([M, Ks], file_out)
