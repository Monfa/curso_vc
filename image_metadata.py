#importing libraries
import cv2 as cv

#loading an image as it is stored in the disk
img = cv.imread('images/pompeii.tif', cv.IMREAD_UNCHANGED) 
#printing image shape
print(img.shape)
print(type(img))
print(img.dtype)
