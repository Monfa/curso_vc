#importing libraries
import cv2 as cv
import numpy as np
import utils as ut

#loading a binary image as it is stored in the disk.
img = cv.imread('images/pompeii.tif', cv.IMREAD_UNCHANGED)

#using the canny detector
img_edges = cv.Canny(img,100,200)

#saving image
cv.imwrite('output/pompeii_edges.png', img_edges)
