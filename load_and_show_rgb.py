#importing libraries
import cv2 as cv

#loading an image as it is stored in the disk.
img = cv.imread('images/street_market_italy.jpg', cv.IMREAD_UNCHANGED)  
cv.imshow('Street Market Italy',img)

#printing metadata
print(img.shape)
print(type(img))
print(img.dtype)

#avoiding closing image after being shown.
#please, select the image and press any keyboard key to close.
cv.waitKey(0)
#closing all opened windows.
cv.destroyAllWindows()