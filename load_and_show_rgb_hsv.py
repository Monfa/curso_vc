#loading an image from disk.
#importing libraries
import cv2 as cv
from matplotlib import pyplot as plt

#loading an image as it is stored in the disk.
img_rgb = cv.imread('images/street_market_italy.jpg', cv.IMREAD_UNCHANGED)
img_hsv = cv.cvtColor(img_rgb, cv.COLOR_BGR2HSV)
img_rgb = cv.cvtColor(img_rgb, cv.COLOR_BGR2RGB)

#now, using matplotlib to show the image
fig = plt.figure()
ax1 = fig.add_subplot(1,2,1)
ax1.imshow(img_rgb)
ax2 = fig.add_subplot(1,2,2)
ax2.imshow(img_hsv)

#show it
plt.show()