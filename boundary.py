#importing libraries
import cv2 as cv
import numpy as np

#loading a binary image as it is stored in the disk.
img = cv.imread('images/binary_sphere.png', cv.IMREAD_UNCHANGED) 

#structuring element
struct_elem = np.array([[0, 1, 0],[1, 1, 1],[0, 1, 0]], np.uint8)

img_bdr = img - cv.erode(img, struct_elem, iterations=1)

#saving images using OpenCV
cv.imwrite('output/boundary.png', img_bdr)