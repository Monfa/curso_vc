#importing libraries
import cv2 as cv
import numpy as np

#loading an image as it is stored in the disk.
im_br = cv.imread('images/pompeii_blurry.png', cv.IMREAD_UNCHANGED)  

#defining laplacian mask:
#mask = np.array([[1, 1, 1],[1, -8, 1],[1, 1, 1]], np.float32)
mask = np.array([[0, 1, 0],[1, -4, 1],[0, 1, 0]], np.float32)

#convolution
im_tr = cv.filter2D(im_br.astype(np.float32),-1,mask)

#summing the result of laplacian and the image
c=-1
im_agc = im_br+c*im_tr

#saving images using OpenCV
cv.imwrite('output/pompeii_sharpened.png', im_agc)