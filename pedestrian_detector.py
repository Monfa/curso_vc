#importing libraries
import cv2 as cv
import imutils as imu
from imutils.object_detection import non_max_suppression
import numpy as np

#loading trained classifier.
hog = cv.HOGDescriptor()
hog.setSVMDetector(cv.HOGDescriptor_getDefaultPeopleDetector())

#loading an image as it is stored in the disk.
img = cv.imread('images/pedestrians.png')

#to reduce computational cost, resizing can be needed
img = imu.resize(img, width=min(400, img.shape[1]))

#detecting people in the image
(rects, weights) = hog.detectMultiScale(img, winStride=(4, 4), padding=(4, 4), scale=1.05)

#filtering detected rectangles by non-maxima suppresion
#large overlap to try to maintain overlapping people
rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
rects_nms = non_max_suppression(rects, probs=None, overlapThresh=0.65)

#drawing the bounding boxes
for (x_a, y_a, x_b, y_b) in rects_nms:
    cv.rectangle(img, (x_a, y_a), (x_b, y_b), (0, 0, 255), 2)

#saving images using OpenCV
cv.imwrite('output/img_ped.png', img)