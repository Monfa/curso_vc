#importing libraries
import numpy as np
import cv2 as cv

#loading an image as it is stored in the disk.
f = cv.imread('images/napoli_mall_low_intensity.png', cv.IMREAD_UNCHANGED)

#converting the BGR image to HSV to apply
#logarithm just in value channel
f_hsv = cv.cvtColor(f, cv.COLOR_BGR2HSV)

#the low epsilon number is used to avoid logarithm of zero
epsilon = np.finfo(float).eps

#logarithm operation through image
L = 256
c = (L-1)/np.log(L-1)
shape_f = np.shape(f)

#initiating the output image
g_hsv = f_hsv
for j in range(0,shape_f[1]):
    for i in range(0,shape_f[0]):
        g_hsv[i,j,2]=c*np.log(f[i,j,2]+epsilon)

#converting back the image to the BGR format
#for exibition purposes
g = cv.cvtColor(g_hsv, cv.COLOR_HSV2BGR)

#saving images using OpenCV
cv.imwrite('output/logarithm_transf.png', g)