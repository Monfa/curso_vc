#importing libraries
import pickle
import cv2 as cv

#loading camera parameters
with open('calibration/camera_params.pkl', 'rb') as file_in:
    M, Ks = pickle.load(file_in)

#image that will be undistorted 
distorted = cv.imread('calibration/distorted.jpg')

#obtaining the best roi to crop undistorted image
#this procedure avoids black borders on the image
h,  w = distorted.shape[:2]
M_new, roi = cv.getOptimalNewCameraMatrix(M,Ks,(w,h),0,(w,h))

#undistort
undistorted = cv.undistort(distorted, M, Ks, None, M_new)

#cropping image
x,y,w,h = roi
undist_cropped = undistorted[y:y+h, x:x+w]

#saving undistorted image
cv.imwrite('calibration/undistorted.png', undist_cropped)