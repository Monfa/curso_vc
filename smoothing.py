#importing libraries
import cv2 as cv
import numpy as np

#loading an image as it is stored in the disk.
f = cv.imread('images/pompeii.png', cv.IMREAD_UNCHANGED)  

#defining mask:
#it is equivalent to:
#mask = np.matrix([[1, 1, 1],[1, 1, 1],[1, 1, 1]],np.float32)/9
mask = np.ones((3,3),np.float32)/9

#convolution
g = cv.filter2D(f.astype(np.float32),-1,mask)

#saving images using OpenCV
cv.imwrite('output/pompeii_blurry.png', g)